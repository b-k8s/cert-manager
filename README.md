# cert-manager

This installs and configures `cert-manager` and sets it up to do LetsEncrypt
certs using DNS01 ACME challenge against AWS Route53

## ACME DNS01 challenge with Route53

Generate the AWS sealedsecret to enable manipulation of route53 records like this:

```
cat creds.csv | tail -1 | cut -f2 -d, |\
  kubectl create secret generic route53-credentials-secret \
  --dry-run=client \
  --namespace cert-manager \
  --from-file=secret-access-key=/dev/stdin \
  -o yaml \
  --type Opaque | \
  kubeseal -o yaml | \
  grep -v creationTimestamp > beast/sealedsecret.yaml
```
